package com.test.testProject.UI.ServiceHelper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;


public class GeneralHelper {
	WebDriver driver;

	//Locators
	//###############################
	public  String username = "//input[contains(@class, 'username')]";
	public  String userpswd = "//input[contains(@class, 'password')]";
	public  String submit_button = "//input[contains(@type , 'submit')]";
	
	
	//#####################################################
	//Methods
	
	/*public GeneralHelper(WebDriver driver) {
		this.driver = driver;
	}*/
	
	public  void loginWithCredential(WebDriver driver, String userName, String paswd)
	{	
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.findElement(By.xpath(username)).sendKeys(userName);
		driver.findElement(By.xpath(userpswd)).sendKeys(paswd);
		driver.findElement(By.xpath(submit_button)).click();
	}
	
	public  void openTab(WebDriver driver, String tab)
	{	
		driver.findElement(By.xpath(tab)).click();
	}
	
}

