package com.test.testProject.UI.TestScript.UI;

import com.test.testProject.UI.ServiceHelper.GeneralHelper;
import com.test.testProject.UI.ServiceHelper.GeneralLocators;
import com.test.testProject.UI.ServiceHelper.SwagIqToolsHelper;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;


public class Rulescreation {

	public  String userName = "mohit@swagiqbeta7.com";
	public  String userPassword = "admin123";
	WebDriver driver = new ChromeDriver();
	GeneralHelper genhel = new GeneralHelper();
	GeneralLocators genloc = new GeneralLocators();
	SwagIqToolsHelper swagiq = new SwagIqToolsHelper();
	
	@Test
	public void rulesCreationandProcessing()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		genhel.loginWithCredential(driver, userName, userPassword);
		genhel.openTab(driver, genloc.tab_swag_iq_tools);
		
		
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		swagiq.click_swag_iq_new_rule(driver);
		swagiq.set_swag_iq_rule_name(driver, "Test Rule");
		swagiq.set_swag_iq_rule_description(driver,"Test Descripption");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		swagiq.select_swag_iq_rule_object(driver, swagiq.SWAG_IQ_select_value_CONTACT);
		swagiq.select_swag_iq_rule_trigger(driver, swagiq.SWAG_IQ_select_value_WORKFLOW);
		swagiq.select_swag_iq_rule_send_type(driver, swagiq.SWAG_IQ_select_value_auto_send);
		swagiq.select_swag_iq_rule_recipient_address(driver, swagiq.SWAG_IQ_select_value_contact_mailing_address);
		
		swagiq.select_swag_iq_select_assignment_type(driver, swagiq.SWAG_IQ_ASSIGNMENT_TYPE_USER);
		/*try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}*/
		//swagiq.select_swag_iq_select_assignment_to(driver);
		
		try {
			
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		//swagiq.click_swag_iq_radio_button_created_everytime_edited(driver);
		swagiq.select_swag_iq_select_rule_evaluation(driver, swagiq.SWAG_IQ_select_value_formula_evaluate_true);
		
		try {
			
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		swagiq.select_swag_iq_activate_rule(driver);
		swagiq.click_swag_iq_rule_gift(driver);
		
		try {
			
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		swagiq.select_swag_iq_select_i_love_swag(driver);
		swagiq.click_swag_iq_save_button(driver);
	}
	
}
